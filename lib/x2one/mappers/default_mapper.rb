module X2one
  module Mappers
    class DefaultMapper
      attr_accessor :context

      def initialize(context = nil)
        @context = context
      end

      def map(data, map)
        (data.is_a? Array) ? map_record(data, map) : map_model(data, map)
      end

      def map_model(model, map)
        return nil if model.nil?

        if map.is_a? Hash
          record = {}
          map.each do | key, value |
            record[key] = map_value(model, key, value)
          end
          return record
        elsif map.is_a? Array
          record = []
          map.each do | value |
            record << map_value(model, nil, value)
          end
          return record
        else
          map_value(model, nil, map)
        end
      end

      def map_value(model, key, value)
        if value.is_a? Hash
          map_model((key.is_a? Symbol) ? model.send(key) : model, value)
        elsif value.is_a? Array
          map_model_collection((key.is_a? Symbol) ? model.send(key) : [model], value ? value[0] : nil)
        elsif value.is_a? Symbol
          map_field(model, value.to_s)
        elsif value.is_a? Mappable
          value.map(model, key, context)
        else
          value
        end
      end

      def map_field(model, fields)
        v = model
        if fields.index('.')
          fields.split('.').each do | field |
            v = v.send(field)
            return v if v.nil?
          end
          v
        else
          v.send(fields)
        end
      end

      def model_value(model, fields)
        v = model
        if fields.index('.')
          fields.split('.').each do | field |
            v = v.send(field)
            return v if v.nil?
          end
          v
        else
          v.send(fields)
        end
      end

      def map_model_collection(collection, map)
        records = []
        if map.nil?
          fields = collection.column_names
          map = Hash[fields.collect { | field | [field, field.to_sym] }]
        end
        collection.each do | model |
          records.push(map_model(model, map))
        end
        records
      end

      def map_record(values, map)
        record = {}
        map.each do | key, value |
          if value.is_a? Hash
            record[key] = map_record(values, value)
          elsif value.is_a? Mappable
            record[key] = value.map(values, key, context)
          elsif value.is_a? Symbol
            record[key] = values[value.to_s.to_i]
          else
            record[key] = value
          end
        end
        record
      end

      private

      def relation?(model, name)
        model.class.reflections.keys.include? name
      end

    end
  end
end
