module X2one
  module Mappers
    class ArrayMapper

      def initialize()
        @mapper = nil
      end

      def map(data, path, actions, mapping)
        data = data.send(path).to_a
        @mapper = (mapping.is_a? Mappable) ? mapping : Mappers::DefaultMapper.new()
        array = []
        array = actions(data, actions)
        array = mapping(array, mapping)
        array
      end


      def actions(data, actions)
        array = []
        actions.each do |action|
          key = action.keys.first
          value = action[action.keys.first]
          if value.is_a? Symbol or value.is_a? String
            array = data.send(key, &value.to_sym)
          elsif value.split("::").first == "Processors"
            array = TypeResolver.resolve_type!(value).new().process({data: data, action: key})
          end
        end
        return array
      end

      def mapping(data, map)
        if map.is_a? Mappable
          entities = map_model(data, map)
        else
          entities = @mapper.map_model(data, map)
        end
        entities
      end

      def map_model(model, map)
        return nil if model.nil?

        if model.is_a? Hash
          # model = (model.is_a? (ActiveRecord::Base)) ? model.attributes : model
          record = {}
          model.each do | key, value |
            record[key] = map_value(map, model, value)
          end
          return record
        elsif model.is_a? (ActiveRecord::Base)
          record = map_value(map, nil, model)
          return record
        elsif model.is_a? Array
          record = []
          model.each do | value |
            record << map_value(map, model, value)
          end
          return record
        end
      end

      # def complex(model)
      #   return true if  model.any? {|k, v| v.is_a? Array or v.is_a? Hash }
      #   return false
      # end


      def map_value(map, model, value)
        if value.is_a? Hash
          map_model(value, map)
        elsif value.is_a? Array
          map_model_collection(value, map)
        else
          @mapper.map(value, map)
        end
      end

      def map_model_collection(collection, map)
        records = []
        collection.each do | model |
          records.push(map_model(model, map))
        end
        records
      end

    end
  end
end
