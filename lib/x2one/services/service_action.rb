module X2one
  module Services
    class ServiceAction
      attr_accessor :name
      attr_accessor :operation
      attr_accessor :data
      attr_accessor :actions

      def initialize(name = nil, op = nil, data = nil, children = nil)
        @name = name
        @operation = op
        @data = data
        @actions = children
      end

      def spread
        actions = []
        @data.each do | item |
          actions.push(ServiceAction.new(self.name, self.operation, item, self.actions))
        end
        actions
      end
    end
  end
end