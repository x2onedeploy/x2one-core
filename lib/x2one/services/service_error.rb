module X2one
  module Services
    class ServiceError < StandardError; end

    class InvalidServiceError < ServiceError;
      def initialize(service)
        super(I18n.t(:'errors.messages.invalid_service', service: service))
      end
    end

    class InvalidServiceActionError < ServiceError;
      def initialize(action)
        super(I18n.t(:'errors.messages.invalid_service_action', action: action))
      end
    end
  end
end