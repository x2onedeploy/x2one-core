module X2one
  module Services
    class ServiceResponse
      attr_accessor :header
      attr_accessor :data

      def initialize(data = nil, component = nil, action = nil, status = nil)
        @header = Header.new({ component_id: component, task_id: action })
        @data = data if !data.nil?
        @header.status = status if status
        #@component = component
        #@action = action
        #@status = status
      end

      def succeeded?
        @header.status.success
      end

      def self.response(request, results)
        response = ServiceResponse.new
        if (request.is_a?(ServiceRequest))
          response.header.component_id = request.component
          response.header.task_id = request.action
        else
          response.header.task_id = request
        end
        response.header.status = results.errors? ? Status.error(results.error_message, results.errors) : Status.ok(results.status_message, results.successes)
        response.data = results.success_data
        #--TEMP (for compatibility) - breaks 'delta'
        response.data = results.success_data.count == 1 ? results.success_data[0]: results.success_data;
        #--TEMP
        response
      end

      def self.error(message = nil, task_info = nil, details = nil, data = nil)
        component, action = task_info.component, task_info.action if (task_info.is_a?(ServiceRequest))
        component, action = nil, task_info if (task_info.is_a?(String))
        ServiceResponse.new(data, component, action, Status.error(message, details))
      end

      def self.success(message = nil, data = nil, task_info = nil)
        component, action = task_info.component, task_info.action if (task_info.is_a?(ServiceRequest))
        component, action = nil, task_info if (task_info.is_a?(String))
        ServiceResponse.new(data, component, action, Status.ok(message))
      end

      def self.empty(action = nil)
        self.success('Empty', [], action)
      end

      def self.not_implemented(action)
        self.error("#{action} is not implemented.")
      end

      def self.entity_not_found(entity_type)
        ServiceResponse.error("#{entity_type} not found.")
      end

      def self.invalid_service(service)
        ServiceResponse.error("Invalid Service: #{service}")
      end

      def as_json(options = nil)
        if data.nil? || options.nil? || data.kind_of?(ActiveRecord::Base)  #backwards compatibility
          super(options)
        else
          jasoned_data = {}
          if (data.kind_of?(Array))   #backwards compatible
            jasoned_data = data.as_json(options)
          else
            data.each do |key, value|
              jasoned_data[key] = data[key].as_json(options[key])
            end
          end
          self.data = jasoned_data
          super({})
        end
      end

    end
  end
end