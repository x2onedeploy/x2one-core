require 'service_error'

module X2one
  module Services
    class ServiceManager
      extend ActiveModel::Callbacks

      attr_accessor :context

      define_model_callbacks :action

      def initialize(context)
        @context = context
        @config = ServiceConfig.config
        @registered_services = @config[:services]
      end

      def self.execute(context, requests)
        manager = self.new(context)
        requests = [requests] if !requests.is_a?(Array)
        responses = manager.execute_requests(requests)
        responses.length == 1 ? responses[0] : responses
      end

      def execute_requests(requests)
        responses = []
        requests.each do | request |
          response = execute_service(request)
          responses.push(response)
        end
        responses
      end

      def execute_service(request)

        can_process!(request)
        service = resolve_service(request.component)
        puts '========== EXECUTE REQUEST ============='
        puts request.inspect
        puts '========================================'
        service.execute(context, request)
      rescue StandardError => e
        ServiceResponse.error(e.message)
      end

      def can_process(request)
        can_process!(request)
        true
      rescue
        false
      end

      def can_process!(request)
        registered!(request.component, request.action)
        authorize_request(request)
      end

      def registered(service, task)
        registered!(service, task)
        true
      rescue
        false
      end

      def registered!(service, task)
        raise InvalidServiceError.new(service) if !registered_services.include?(service)
        raise InvalidServiceActionError, "Invalid Service Action: #{task}" if !registered_services[service].include?(task.to_sym)
      end

      def authorize_request(request)
        operation = request.operation
        operation = Services.read_operations.include?(operation) ? :read : operation
        role = context.current_role
        raise CanCan::AccessDenied.new(Localizer.localize :'errors.messages.access_denied') if (
        role.cannot?(:manage, request.component) &&
            role.cannot?(operation, request.name) &&
            role.cannot?(operation, request.type_name)
        )
      end

      def get_service(service_name)
        resolve_service(service_name)
      end

      protected

      attr_accessor :registered_services
      attr_accessor :config

      def resolve_service(service_name)
        resolve_service!(service_name)
      rescue
        EntityService
      end

      def resolve_service!(service_name)
        service_class = "#{service_name.to_s.camelize}Service"
        X2one::TypeResolver.resolve_type!(service_class)
      rescue
        raise InvalidServiceError, service_name
      end

    end
  end
end
