module X2one
  module Services
    class ServiceBase
      extend ActiveModel::Callbacks

      attr_accessor :context
      attr_accessor :request

      define_model_callbacks :action
      define_model_callbacks :process_request

      def self.execute(context, request)
        self.new(context, request).action
      end

      def self.can_process(action)
        self.new(nil, nil).can_process(action)
      end

      def initialize(context, request)
        @context = context
        @request = request
        @actions = registered_actions
      end

      def can_process(action)
        actions.include?(action)
      end

      def action
        run_callbacks :action do
          method_name = request.action
          return ServiceResponse.invalid_service(method_name) if (method_name.nil?)

          #if can_process(method_name.to_sym)
          begin
            method = resolveTask(method_name)
            if (method)
              method.call(request)
            else
              process_request(request)
            end
          rescue StandardError => err
            Rails.logger.error err.message
            Rails.logger.error err.backtrace
            ServiceResponse.error(err.message, request)
          end
          #else
          #  ServiceResponse.invalid_service(method_name)
          #end
        end
      end

      protected

      attr_accessor :actions

      def registered_actions
        []
      end

      def service_response(type, data, task = caller_name)
        ServiceResponse.success(type.name.demodulize.pluralize, data, task)
      end

      def ok_response(message = nil, data = nil, task = nil)
        ServiceResponse.success(message, data, task)
      end

      def error_response(message = nil, task = nil, details = nil)
        ServiceResponse.error(message, task, details)
      end

      def process_request(request, processors = nil)

        processors ||= X2one::Processors::ProcessorFactory.get_request_processors(request, context)
        results = X2one::Processors::Results.new
        processors.each do | processor |
          process_results = processor.process(request)
          results.merge(process_results)
        end
        ServiceResponse.response(request, results)
      end

      #------backwards compatibility / convenience
      def process_get(type, entities, task); process_entities(:get, type, entities, task) end
      def process_save(type, entities, task); process_entities(:save, type, entities, task) end
      def process_delete(type, entities, task); process_entities(:destroy, type, entities, task) end
      def process_create(type, entity, task); process_entities(:save, type, entity, task) end
      def process_update(type, entity, task); process_entities(:save, type, entity, task) end
      def process_entities(action, type, entities, task)
        results = X2one::Processors::EntityProcessor.new.process(ServiceAction.new(type, action, entities))
        ServiceResponse.response(task, results)
      end
      def save_entity(entity); X2one::Processors::EntityProcessor.new.save_entity(entity) end
      def delete_entity(entity); X2one::Processors::EntityProcessor.new.delete_entity(entity) end
      #----------

      private

      def caller_name
        caller_locations(2,1)[0].label
      end

      def resolveTask(action_name)
        self.method(action_name)
      rescue
        return nil
        #raise InvalidServiceActionError, action_name
      end

    end
  end
end
