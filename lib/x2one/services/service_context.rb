module X2one
  module Services
    class ServiceContext
      attr_accessor :current_user
      attr_accessor :current_domain_user
      attr_accessor :current_profile
      attr_accessor :current_role
      attr_accessor :role_type
      attr_accessor :principals

      def initialize(context)
        @current_user = context.user
        @current_profile = context.profile
        @current_role = context.role
        @current_domain_user = context.domain_user
        @role_type = context.role_type
        @principals = context.principals
      end
    end
  end
end
