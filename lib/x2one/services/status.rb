module X2one
  module Services
    class Status
      attr_accessor :code
      attr_accessor :message
      attr_accessor :detail
      attr_accessor :success

      def initialize(code, message, detail)
        @code = code
        @success = @code == :success
        @message = Localizer.localize(message)
        @detail = detail
      end

      def self.error(message = nil, detail = nil)
        Status.new(:error, message || 'Action Failed.', detail)
      end

      def self.ok(message = nil, detail = nil)
        Status.new(:success, message || 'Action was Successful.', detail)
      end
    end
  end
end
