module X2one
  module Services
    class ErrorDetail
      attr_accessor :message
      attr_accessor :data

      def initialize(message, data = nil)
        @message = message
        @data = data
      end

      def to_s
        @message
      end
    end
  end
end

