module X2one
  module Services
    class << self; attr_reader :read_operations; end
    @read_operations = [:get, :exists]
  end
end
