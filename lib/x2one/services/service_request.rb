require 'service_error'

module X2one
  module Services
    class << self; attr_reader :read_operations; end
    @read_operations = [:get, :exists]

    class ServiceRequest
      attr_accessor :component
      attr_accessor :action
      attr_accessor :data
      attr_accessor :actions

      public

      def self.request(request)

        component = request[:header][:component_id]
        raise InvalidServiceError, "Invalid Service: #{component}" if component.nil?
        action = request[:header][:task_id]
        raise InvalidServiceActionError, "Invalid Service Action: #{action}" if action.nil?
        service_request = ServiceRequest.new(component.to_sym, action.to_sym, request[:data])
        #service_request.data = service_request.build_service_data(request[:data])
        # request[:actions].each do | action |
        #   service_request.actions.push(ServiceRequest.request(action))
        # end
        service_request
      end

      def self.build_request(component, action, data = nil, subactions = nil)
        service_request = ServiceRequest.new(component, action, data)
        #service_request.data = service_request.build_service_data(data)
        # subactions.each do | action |
        #    service_request.actions.push(ServiceRequest.request(action))
        # end
        service_request
      end

      def operation
        @op_info[:operation]
      end

      def name
        @op_info[:name]
      end

      def type_name
        @op_info[:type_name]
      end

      private
      @op_info

      def initialize(component, action, data = nil, actions = nil)

        @component = component
        @action = action
        @op_info = op_info(component, action)
        @data = build_service_data(data)
        @actions = actions
      end

      def op_info(component, action)
        action_parts = action.to_s.rpartition('_')
        name = action_parts.first.empty? ? component : action_parts.first.to_sym
        type = action_parts.first.empty? ? component : action_parts.first.singularize.to_sym
        operation = action_parts.last.to_sym
        {type_name: type, name: name, operation: operation }
      end

      def build_service_data(params)
        if X2one::Services.read_operations.include?(self.operation) || self.name == :criteria
          parms = params.nil? ? {} : (params.is_a?(ActionController::Parameters)) ? params.fetch(:criteria) : params[:criteria]
          type = :criteria
        else
          parms = params.nil? ? {} : (params.is_a?(ActionController::Parameters)) ? params.require(self.name) : params[self.name].nil? ? params[self.action.to_sym] : params[self.name]
          type = self.type_name
        end
        Entities::ServiceData.create(type, parms)
      end
    end
  end
end
