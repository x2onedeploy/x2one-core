module X2one
  module Services
    class Header
      attr_accessor :component_id
      attr_accessor :task_id
      attr_accessor :status
      #attr_accessor :logical_id
      #attr_accessor :language
      #attr_accessor :timestamp
      #attr_accessor :criteria

      def initialize(options = nil)
        options ||= {}
        @component_id = options[:component_id] || 'API'
        @task_id = options[:task_id]
        @status = options[:status] || Status::ok
        #@logical_id = options[:logical_id] || 'X2ONE'
        #@criteria = options[:criteria]
        #@language = options[:language] || 'en'
        #@timestamp = options[:timestamp] || Time.now
      end
    end
  end
end
