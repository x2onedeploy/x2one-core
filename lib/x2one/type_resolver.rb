module X2one
  module TypeResolver
    INHERIT_TYPES = true
    DO_NOT_INHERIT_TYPES = false

    def self.resolve_type(name)
      self.resolve_type!(name)
    rescue
      nil
    end

    def self.resolve_type!(name)
      type_names = name.split('::')
      type = Object
      type_names.each do | type_name |
        type = type.const_get(type_name, DO_NOT_INHERIT_TYPES)
      end
      type
    rescue
      raise TypeError, I18n.t('errors.messages.invalid_type', name: name)
    end

    def self.find_type(paths)
      paths = [paths] if !paths.is_a?(Array)
      type = nil
      paths.each do | path |
        type = self.resolve_type(path)
        break if (type)
      end
      type
    end

    def self.type_name(name)
      parts = name.to_s.split('::')
      parts.each_with_index do |part, index|
        parts[index] = part.camelize
      end
      parts.join('::')
    end

    def self.resolve_entity(entity_name, namespaces = [''])
      type_name = entity_name.camelize.singularize
      namespaces.each do | namespace |
        begin
          path = namespace.empty? ? type_name : "#{namespace}::#{type_name}"
          return resolve_type!(path)
        rescue
        end
      end
      raise TypeError, I18n.t('errors.messages.invalid_entity', name: entity_name)
    end

    def self.resolve_domain_type(entity_type)
      instance = entity_type.kind_of?(Array) ? entity_type[0] : entity_type
      is_entity = instance.kind_of?(Entities::ServiceData)
      entity_name = is_entity ? entity_type.type_name : instance.to_s.singularize
      return TypeResolver.resolve_entity(entity_name, ['', 'Billing', 'Payment','Hlr','Routing','Dropdown'])
    end

  end
end