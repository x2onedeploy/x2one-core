module X2one
  module Entities
    class GenericEntity < OpenStruct

      def select_fields(fields)
        to_h.select { |k,v| fields.include? k }
      end

      def get(field)
        return self[field]
      end

    end
  end
end
