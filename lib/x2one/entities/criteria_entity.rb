module X2one
  module Entities
    class CriteriaData < ServiceData
      def initialize(*args)
        super(*args)
        self.action = :read
        self.members.each { |field| self[field] = nil }
      end
    end

    CriteriaEntity = CriteriaData.new(:type, :name, :fields, :params, :conditions, :grouping, :having, :sort, :limit, :offset,
                                      :joins, :includes, :map, :set_name, :select_type, :entity_type, :distinct, :results, :extends, :transforms)

  end
end


