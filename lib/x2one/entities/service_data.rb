module X2one
  module Entities
    class ServiceData < Struct

      attr_accessor :action
      cattr_accessor :polymorphic_field
      cattr_accessor :domain_type

      def initialize(*args)
        super(*args)
        self.action = :none
        self.members.each_with_index { |field, index|
          self[field] = index <= args.length - 1 ?  args[index] : :__undefined__
        }
      end

      def deep_hash(fields = nil)
        struct_to_hash(self, fields)
      end

      def self.declare(fields, polymorphic_field = nil)
        type = ServiceData.new(*fields)
        if polymorphic_field
          type.polymorphic_field = polymorphic_field
        end
        type
      end

      def self.create(type_name, data)
        if (data.kind_of?(Array))
          Entities::ServiceData.create_entities(type_name, data)
        else
          Entities::ServiceData.create_entity(type_name, data.to_h)
        end
      end

      def has?(field)
        return self.members.include?(field)
      end

      def undefined?(field)
        return self[field] == :__undefined__
      end

      def present?(field)
        return !self.undefined?(field) && self[field].present?
      end

      def type_name
        polymorphic? && self.present?(self.polymorphic_field) ?
            self[self.polymorphic_field] :
            self.domain_type.present? ?
                self.domain_type :
                self.class.name.demodulize.chomp('Entity')
      end

      def polymorphic?
        return !self.polymorphic_field.nil?
      end

      def get(field)
        return has?(field) ? self[field] : nil
      end

      def children
        fields = to_h.select do |k,v|
          v.is_a?(Array) || v.is_a?(Entities::ServiceData)
        end
      end

      def initialized_fields
        to_h.select { |k,v| v != :__undefined__ }
      end

      def select_fields(fields, only_initialized = true)
        fields = to_h.select do |k,v|
          if !fields.include? k
            false
          elsif only_initialized && v == :__undefined__
            false
          elsif k.to_s.end_with?('_attributes') && v == nil
            false
          else
            true
          end
        end
        only_initialized ? fields : nullify(fields)
      end

      def nullify(fields)
        fields.each { | k, v, | v == :__undefined__ ? fields[k] = nil : fields[k] = v }
      end

      def exclude(fields)
        return to_h.reject { |k,v| !fields.include? k }
      end

      def deletions?
        #return true if self.deletion?
        self.children.each_value do | value |
          if value.is_a?(Array)
            return true if value.any? { |child| child.deletion? }
          else
            return true if value.deletion?
          end
        end
        false
      end

      def deletions
        d = []
        self.children.each_value do | value |
          if value.is_a?(Array)
            d.concat(value.select { |child| child.deletion? })
          else
            d.push(value) if value.deletion?
          end
        end
        d
      end

      def deletion?
        action == :destroy
      end

      private

      def self.create_entities(type_name, array)
        data = []
        array.each do |item|
          data.push self.create(type_name, item.to_h)
        end
        data
      end

      def self.create_entity(type_name, hash)
        if (generic_type? type_name)
          service_data = generic_type(hash)
        else
          type = entity_type(type_name)
          service_data = hash_to_struct(type, hash)
        end
        service_data
      end

      def struct_to_hash(type, fields = nil)
        type.members.each do | m |
          if (type[m].is_a?(Array))
            arr = Array.new
            type[m].each do | a |
              value = a.is_a?(Hash) ? a : struct_to_hash(a)
              arr.push(value)
            end
            type[m] = arr
          elsif (!type[m].nil?) && type[m].respond_to?('to_h')
            type[m] = struct_to_hash(type[m])
          end
        end
        h = fields ? type.select_fields(fields) : type.initialized_fields
      end

      def self.hash_to_struct(type, hash)
        obj = type.new
        obj.action = hash.delete(:__MARKED__).downcase.to_sym if hash.has_key?(:__MARKED__)
        hash.each do |key, value|
          if (obj.has?(key.to_sym))
            field = key
          elsif ((obj.has?("#{key}_attributes".to_sym)))
            field = "#{key}_attributes"
            value = value.nil? ? [] : value
          else
            next
          end

          if (parseable?(field) && value.is_a?(Array))
            arr = Array.new
            value.each do | v |
              type2 = self.entity_type(key)
              arr.push(hash_to_struct(type2, v.to_h))
            end
            obj[field] = arr
          elsif (parseable?(field) && value.is_a?(Hash))
            if (generic_type? field)
              obj[field] = generic_type(value)
            else
              type2 = self.entity_type(field)
              obj[field] = hash_to_struct(type2, value)
            end
          else
            obj[field] = value
          end
        end
        puts obj
        obj
      end

      def self.entity_type(type_name)
        str = type_name.to_s
        str = str.gsub('_attributes', '')
        str = str.gsub('_', ' ')
        str = str.split.map(&:capitalize).join('')
        type = X2one::TypeResolver.resolve_type("X2one::Entities::#{str}Entity")
        type = X2one::TypeResolver.resolve_type!("X2one::Entities::#{str.singularize}Entity") if type.nil?
        type
      end

      def self.generic_type(hash)
        return GenericEntity.new if hash.nil? || hash.empty?

        fields = []
        hash.keys.each do | k |
          fields.push(k.to_sym)
        end
        criteria = ServiceData.new(*fields)
        service_data = criteria.new(*hash.values)
      end

      def self.generic_type?(field)
        [:params].include? field.to_sym
      end

      def self.parseable?(field)
        [:upload].exclude? field.to_sym
      end

    end
  end
end