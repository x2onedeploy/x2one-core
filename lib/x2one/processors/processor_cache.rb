module X2one
  module Processors
    class ProcessorCache
      @cache = {}

      class << self

        public

        def has?(cache_id)
          cache.key?(cache_id)
        end

        def get(cache_id)
          cache[cache_id]
        end

        def set(cache_id, processor)
          cache[cache_id] = processor
        end

        private

        attr_accessor :cache

      end

    end
  end
end

