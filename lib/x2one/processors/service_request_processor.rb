module X2one
  module Processors
    class ServiceRequestProcessor < Processor

      attr_accessor :name
      attr_accessor :operation
      attr_accessor :sub_actions

      def process(service_request)
        self.name = service_request.name
        self.operation = service_request.operation
        self.sub_actions = service_request.actions

        super(service_request.data)
      end

    end
  end
end

