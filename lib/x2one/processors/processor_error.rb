module X2one
  module Processors
    class ProcessorError < StandardError; end

    class InvalidProcessorError < ProcessorError;
      def initialize()
        super(I18n.t('errors.messages.invalid_processor'))
      end
    end

  end
end
