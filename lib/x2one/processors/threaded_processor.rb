module X2one
  module Processors
    class ThreadedProcessor < SingleValueProcessor

      def intialize(context, config)
        super(context, config)
        #Thread.abort_on_exception = true
      end

      protected
      attr_accessor :threads
      attr_accessor :stopping
      attr_accessor :result
      attr_accessor :default_processor

      def process_input(processors_info)
        initial_result(processors_info)
        self.threads = []

        semaphore = Mutex.new
        self.stopping = false
        begin
          set_default_processor

          processors_info.each_with_index do |processor_info, index|
            self.threads[index] = Thread.new do
              Thread.current.priority = 0 - index
              processor = ProcessorFactory.get_processor(processor_info[:component], processor_info[:processor], self.default_processor)
              raise ProcessorNotFound.new if processor.nil?
              Rails.logger.info "Run Processor #{index}: #{processor} ... "
              input = processor_info[:data] || processor_info
              ActiveRecord::Base.connection_pool.with_connection do
                result = processor.new(context, processor_info[:config] || config[:default_config]).process(input)
                if (!self.stopping)
                  semaphore.synchronize {
                    set_result(index, result)
                    stop if (interrupt?)
                  }
                end
              end
            end
          end
          self.threads.each(&:join)

          self.results = reduce
          Rails.logger.info "Processor #{self.class.name} result is: #{result.to_json} "
        rescue Exception => err
          self.results = Result.error(err.message)
        end
      end

      def initial_result(input)
        self.result = nil
      end

      def set_result(index, result)
        puts("-------SET RESULT WITH #{index} #{result}")
        self.result = result
      end

      def interrupt?
        false
      end

      def reduce
        result
      end

      def stop
        self.stopping = true
        puts "STOP THE THREADS..."
        self.threads.each { |thread| thread.exit }
        puts "STOPPED THE THREADS..."
      end

      # def running?
      #   self.threads.any? { |thread| thread.status }
      # end

      # def get_result
      #   puts "GET THE RESULT..."
      #   while (running?)
      #     stop if (interrupt?)
      #   end
      #   self.results = reduce
      # end

      private
      def set_default_processor
        self.default_processor = nil
        if config[:default_processor]
          if (config[:default_processor].is_a?(Hash))
            self.default_processor = ProcessorFactory.get_processor(config[:default_processor][:component], config[:default_processor][:processor])
          elsif (config[:default_processor] < Processors::Processor)
            self.default_processor = config[:default_processor]
          end
        end
      end

    end
  end
end
