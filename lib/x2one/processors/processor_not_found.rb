module X2one
  module Processors
    class ProcessorNotFound < ProcessorError;
      def initialize()
        super(I18n.t('errors.messages.processor_not_found'))
      end
    end
  end
end