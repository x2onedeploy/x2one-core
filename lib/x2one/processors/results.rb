module X2one
  module Processors
    class Results
      private

      attr_reader :results

      public

      def initialize()
        @results = []
      end

      def errors?
        results.each do | result |
          if (result.status == :error)
            return true
          end
        end
        return false
      end

      def succeeded?
        !errors?
      end

      def first
        results.first
      end

      def all
        results
      end

      def errors
        filter_results(:error)
      end

      def successes
        filter_results(:success)
      end

      def data
        filter_data
      end

      def success_data
        filter_data(:success)
      end

      def error_data
        filter_data(:error)
      end

      def error_message
        msg = ''
        if errors?
          #msg = "Errors Ocurred: #{errors[0].message}"
          msg = I18n.t('errors.messages.errors_occurred') #general message for time being
        end
        msg
      end

      def status_message
        results.length > 0 ? results[0].message : ''
      end

      def error(message, data)
        result(Result.error(message, data))
      end

      def success(message, data)
        result(Result.success(message, data))
      end

      def result(result)
        results.push(result)
      end

      def merge(results)
        @results.concat(results.all)
      end



      private

      def filter_results(status = :all)
        results.select { |result| (result.status == status || status == :all) }
      end

      def filter_data(status = :all)
        filtered_results = filter_results(status)
        filtered_results.collect { |result| result.data }
      end

    end
  end
end
