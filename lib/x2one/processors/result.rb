module X2one
  module Processors
    class Result
      attr_accessor :status
      attr_accessor :message
      attr_accessor :data
      attr_accessor :name
      attr_accessor :action
      attr_accessor :entity

      def self.error(message = nil, data = nil, options = {})
        Result.new(:error, message, data, options)
      end

      def self.success(message = nil, data = nil, options = {})
        Result.new(:success, message, data, options)
      end

      def initialize(status, message = nil, data = nil, options = {})
        @status = status
        @message = Localizer.localize(message)
        @data = data
        @name = options[:name]
        @action = options[:action]
        @entity = options[:entity]
      end

      def to_s
        @message
      end

    end
  end
end
