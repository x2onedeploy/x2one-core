module X2one
  module Processors
    class ProcessorFactory

      class << self

        def get_request_processors(request, context)

          processor_types = []
          cache_id = "#{request.component}::#{request.action}"
          if ProcessorCache.has?(cache_id)
            processor_types = ProcessorCache.get(cache_id)
          else
            processors_config = get_processors_config(request)
            if (processors_config)
              processor_types = get_processors(processors_config)
            else
              paths = get_request_processor_paths(request)
              processor = TypeResolver.find_type(paths)
              if (processor)
                processor_types.push({processor: processor})
              else
                processor_types = get_default_processors(request)
                raise ProcessorNotFound if processor_types.empty?
              end
            end
            ProcessorCache.set(cache_id, processor_types) if (processor_types.any?)
          end

          processors = []
          processor_types.each do | processor_type |
            processor = processor_type[:processor]
            config = processor_type[:config]

            processors.push(processor.new(context, config))
          end

          processors
        end

        def get_processors(processors_config)
          processors = []
          return processors if processors_config.nil?
          processors_config = [processors_config] if !processors_config.is_a?(Array)
          processors_config.each do | processor_config |
            if (processor_config.is_a?(Hash))
              processor = processor_config[:processor]
              config = processor_config[:config]
            else
              processor = processor_config
              config = nil
            end

            if processor.is_a?(Symbol)
              standard_config = ServiceConfig.config[:standard_processors][processor]
              if (standard_config)
                merged_configs = merge_config(standard_config, config)
                processors.concat(get_processors(merged_configs)) if (merged_configs)
              end
            elsif processor.is_a?(String)
              processor = TypeResolver.resolve_type(path)
              processors.push({ processor: processor, config: config})
            elsif processor < X2one::Processors::Processor
              processors.push({ processor: processor, config: config})
            end
          end
          processors
        end

        def merge_config(processor_configs, config)
          sub_configs = []
          processor_configs.each do | sc |
            sub_config = {}
            sub_config[:processor] = (sc.is_a? Hash) ? sc[:processor] : sc
            sub_config[:config] = {}
            sc2 = (sc.is_a? Hash) ? (sc[:config] || {}) : {}
            c = config || {}
            keys = sc2.keys | c.keys
            keys.each do | key |
              if (!sc2[key].nil?)
                sub_config[:config][key] = (sc2[key].is_a? Hash) ? sc2[key].merge(c[key] || {}) : sc2[key]
              else
                sub_config[:config][key] = (c[key].is_a? Hash) ? c[key].merge(sc2[key] || {}) : c[key]
              end
            end
            sub_configs.push(sub_config)
          end
          sub_configs
        end

        def get_processors_config(request)
          processors_config = nil
          component = ServiceConfig.config[:services][request.component.to_sym]
          if (component)
            action = component[request.action] || component[request.action.to_sym]
            if (action)
              processors_config = action[:processors]
            end
          end
          processors_config
        end

        def find_processor(parts)
          paths = []
          parts.each do |part|
            paths << get_processor_path(part[:component], part[:processor])
          end
          TypeResolver.find_type(paths)
        end

        def get_processor(component, name, default = nil, path = nil)
          self.get_processor!(component, name, path)
        rescue
          default
        end

        def get_processor!(component, name, path = nil)
          raise InvalidProcessorError if name.nil?
          cache_id = "#{component}::#{name}"
          if ProcessorCache.has?(cache_id)
            ProcessorCache.get(cache_id)
          else
            path = get_processor_path(component, name) if path.nil?
            processor = TypeResolver.resolve_type!(path)
            ProcessorCache.set(cache_id, processor) if (processor)
            processor
          end
        end

        private

        def get_processor_path(component, name)
          namespace = 'Processors::'
          name = TypeResolver.type_name(name)
          if (component)
            component = TypeResolver.type_name(component)
            namespace = "#{namespace}#{component}::"
          end
          "#{namespace}#{name}Processor"
        end

        def get_request_processor_paths(request)
          task = TypeResolver.type_name(request.action)
          component = TypeResolver.type_name(request.component)
          type = TypeResolver.type_name(request.type_name)
          operation = TypeResolver.type_name(request.operation)

          paths = []
          paths.push("Processors::#{component}::#{task}Processor")
          paths.push("Processors::#{component}::#{type}#{operation}Processor")
          paths.push("Processors::#{component}::#{type}Processor")
          paths.push("Processors::#{component}::#{component}Processor")
          paths.push("Processors::#{component}::#{operation}Processor")
          paths
        end

        def get_default_processors(request)
          default_processor = ServiceConfig.config[:default_processors][request.operation]
          get_processors(default_processor)
        end

      end

    end
  end
end

