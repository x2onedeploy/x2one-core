module X2one
  module Processors
    class BooleanProcessor < ThreadedProcessor
      attr_accessor :logical_type  #[:all, :any]
      attr_accessor :boolean_type  #[true, false]

      def initialize(context = nil, config = { logical_type: :any, boolean_type: true })
        super(context, config)
        self.logical_type = config && config[:logical_type] || :any
        self.boolean_type = config && config[:boolean_type] == true
      end

      protected

      def initial_result(input)
        self.result = self.boolean_type
      end

      def set_result(index, result)
        puts("-------SET RESULT WITH #{result.data}")
        if (boolean_type)
          self.result = logical_type == :all ? (self.result && result.data) : (self.result || result.data)
        else
          self.result = logical_type == :all ? (self.result || result.data) : (self.result && result.data)
        end
      end

      def interrupt?
        if (self.boolean_type)
          logical_type == :all ? !(self.result && true) : (self.result || false)
        else
          logical_type == :all ? (self.result || false) : !(self.result && true)
        end
      end

      def reduce
        (self.boolean_type) ? self.result : !self.result
      end

    end
  end
end
