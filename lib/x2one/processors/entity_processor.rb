module X2one
  module Processors
    class EntityProcessor < ServiceRequestProcessor
      extend ActiveModel::Callbacks

      define_model_callbacks :process_entity

      def initialize(context = nil, config = {})

        super(context, config)
        # @permissions_enabled = config.present? && config[:permissions_enabled].nil? ? true : config[:permissions_enabled]
      end

      public
      #convenience
      def create_entity(domain_entity); process_entity(domain_entity, :create) end
      def update_entity(domain_entity); process_entity(domain_entity, :update) end
      def save_entity(domain_entity); process_entity(domain_entity, :save) end
      def delete_entity(domain_entity); process_entity(domain_entity, :destroy) end
      def save_entities(service_entities); process_entities(:save, service_entities) end
      def delete_entities(type, service_entities); process_entities(:destroy, service_entities) end #TODO

      protected

      attr_accessor :rollback

      def process_input(input)
        #domain_type = TypeResolver.resolve_domain_type(self.name)
        entities = input

        if (self.operation == :exists)
          domain_type = TypeResolver.resolve_domain_type(self.name)
          entities_exists(domain_type, entities)
        else
          process_entities(self.operation, entities)
        end
      end

      def process_entity?
        if @permissions_enabled
          case @action
            when :save
              can = @domain_entity.modifiable?(context.principals, @scope)
            when :destroy
              can = @domain_entity.deletable?(context.principals, @scope)
          end
        else
          can = true
        end
        self.results.error(Localizer.localize(:'errors.messages.unauthorized_action', action: @action), @domain_entity) if !can
        can
      end

      def entities_exists(type, criteria)
        begin
          criteria = criteria.kind_of?(Array) ? criteria[0] : criteria
          data = { };
          exists = !type.where(criteria.params.to_h).empty?
          name = :exists
          data[name] = exists
          @results.result(Result.success("#{type.name.demodulize.pluralize} were successfully checked.", data, name))
        rescue StandardError => err
          Rails.logger.error err.message
          @results.error(err.message, criteria)
        end
      end

      def process_entities(action, entities, scope = nil)
        action = :save if [:create, :update].include? action
        raise Localizer.localize(:'errors.messages.invalid_action', action: action) if ![:save, :destroy].include? action

        @scope = scope
        @action = action
        entities = [entities] if !entities.kind_of?(Array)
        entities.each do |entity|
          begin
            @service_entity = entity
            @domain_entity = populate_domain_entity(@service_entity, nil, @action)  #TODO
            if process_entity?
              run_callbacks :process_entity do
                #@result = process_entity(@domain_entity, @action, @service_entity) //TODO
                @result = process_entity(@domain_entity, @action)
                if action == :save && @service_entity.deletions?
                  process_entities(:destroy, @service_entity.deletions, @domain_entity)
                end
              end
              @results.result(@result)
            end
          rescue StandardError => err
            Rails.logger.error err.message
            @results.error(err.message, @service_entity)
            raise ActiveRecord::Rollback if (rollback)
          end
        end
      end

      def process_entity(model, action, entity = nil, rollback = false)
        model = populate_domain_entity(entity, model, action) if (action != :destroy) if (entity)
        #if !model.public_send("secure_#{action}".to_sym, context.principals)
        state = action == :destroy ? "Deleted" : model.id.nil? ? "Created" : "Updated"
        if !model.public_send(action)
          error_message = model.errors.empty? ? Localizer.localize(:'statuses.messages.action_failed', action: action, entity: model.class.name) : model.errors.full_messages.to_sentence
          Result.error(error_message, model)
        else
          Result.success({message: :'statuses.messages.action_succeeded', action: action, entity: model.class.name}, model,  { action: state, entity: model.class.name })
        end
      end

      private

      def populate_domain_entity(service_entity, domain_entity = nil, action = :save)
        #return nil if service_entity.action != action
        raise Localizer.localize(:'errors.messages.invalid_mark') if service_entity.action != action
        domain_entity = domain_entity || get_domain_entity(service_entity)
        #domain_entity = get_domain_entity(service_entity)
        fields = processible_fields(domain_entity)
        values = service_entity.select_fields(fields)
        domain_entity.attributes = values
        service_entity.children.each do | field, value |
          if value.is_a?(Array)
            value.each do | child |
              if (child.action == action)
                #domain_entity.send(field).build(populate_domain_entity(child).attributes)
                domain_entity.send(field).push(populate_domain_entity(child, nil, action))
              end
            end
          else
            domain_entity.send("#{field}=", get_domain_entity(value))
          end
        end
        domain_entity
      end

      def get_domain_entity(service_entity, action = nil, type = nil)
        type = TypeResolver.resolve_domain_type(service_entity) if type.nil?
        domain_entity = (service_entity.present?(:id) ? type.find_by_id(service_entity.id) : type.new)
        #domain_entity = (action == :destroy) ? type.find_by_id(service_entity.id) : (service_entity.id ? type.find_by_id(service_entity.id) : type.new)
        raise Localizer.localize(:'errors.messages.cannot_operate_on', action: action, entity: type.name.demodulize, id: service_entity.id) if !domain_entity
        domain_entity
      end

      def processible_fields(model, include_attributes = false)
        fields = model.class.column_names.map(&:to_sym)
        if (include_attributes)
          fields = fields.concat( model.class.reflections.keys.map {|x| "#{x}_attributes".to_sym})
        end
        fields
      end

    end
  end
end
