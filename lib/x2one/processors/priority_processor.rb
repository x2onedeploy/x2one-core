module X2one
  module Processors
    class PriorityProcessor < ThreadedProcessor
      attr_accessor :highest_priority

      def initial_result(input)
        self.result = Array.new(input.length) {|index| { priority: index, status: nil, value: nil } }
        self.highest_priority = 0
      end

      def set_result(index, result)
        self.result[index][:status] = result.status
        self.result[index][:value] = result.data

        # priority_results = self.result.select { |result| result[:value] == true || result[:status].nil? }
        priority_results = self.result.select { |result| (result[:value].present? && result[:status] == :success) || result[:status].nil? }
        self.highest_priority = priority_results.first[:priority] || self.highest_priority
        puts "INDEX #{index} VALUE = #{self.result[index][:value]}"
        puts "HIGHEST = #{self.highest_priority}"
      end

      def interrupt?
        return self.result[highest_priority][:value]
      end

      def reduce
        self.result[highest_priority]
      end

    end
  end
end

