require_relative 'processor_error'

module X2one
  module Processors
    class Processor
      extend ActiveModel::Callbacks

      define_model_callbacks :process

      attr_accessor :context
      attr_accessor :config
      attr_accessor :results

      def initialize(context = nil, config = {})
        @context = context
        @config = config || {}
      end

      def process(input)
        self.results = initialize_results
        if (process?(input))
          run_callbacks :process do
            process_input(input)
          end
        end
        Rails.logger.info(self.results)
        self.results
      end

      protected

      def initialize_results
        Results.new
      end

      def process?(input)
        true
      end

      def process_input(input)

      end
    end
  end
end
