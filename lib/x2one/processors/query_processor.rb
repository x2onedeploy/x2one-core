module X2one
  module Processors
    class QueryProcessor < X2one::Processors::ServiceRequestProcessor
      #class QueryProcessor < Processors::ServiceRequestProcessor
      attr_accessor :named_queries

      def initialize(context = nil, config = {})
        super(context, config)
        @named_queries = config && config[:queries] ? config[:queries] : {}
        @resolver = config && config[:resolver] ? config[:resolver] : Resolvers::ParameterResolver.new(context)
        @mapper = config && config[:mapper] ? config[:mapper] : Mappers::DefaultMapper.new(context)
      end

      def process(service_action)
        service_action.data = [service_action.data] if !(service_action.data).kind_of? (Array)
        super(service_action)
      end

      def process_input(criteria_list)
        sets = {}
        cl = get_queries(criteria_list)
        cl.each do | criteria |
          case (criteria.type)
            when CriteriaType::NEW_ITEM
              data = new_item(criteria)
            else
              data = query(criteria)
          end
          sets = sets.merge(data)
        end
        results.result(Result.success(:'statuses.messages.query_succeeded', sets))
      end

      def default(criteria)
        query(criteria)
      end

      private

      def new_item(criteria)
        domain_type = TypeResolver.resolve_domain_type(criteria.entity_type || self.name)
        model = domain_type.respond_to?('default') ? domain_type.default(criteria.params) : domain_type.new(criteria.params.to_h)
        fields_map = criteria.map || default_map(domain_type.column_names.map(&:to_sym), :model) #Hash[*(domain_type.column_names).zip([]).flatten]
        { default_item: @mapper.map(model, fields_map)  }
      end

      def query(criteria)
        domain_type = TypeResolver.resolve_domain_type(criteria.entity_type || self.name)
        execute_query(domain_type, criteria)
      end

      def get_queries(criteria_list)
        queries = []
        criteria_list.each do | criteria |
          criteria_type = criteria.type || ''
          #query_name = criteria.name ?  "#{criteria.name}_#{criteria_type}".to_sym : criteria_type.to_sym
          query_name = criteria.name ?  criteria.name.to_sym : criteria_type.to_sym
          nqs = get_named_queries(query_name)
          if nqs.empty?
            queries.push(criteria)
          else
            nqs.each do | query |
              query = extend_query(query.clone) if query[:extends]
              queries.push(merge_criteria(criteria, query))
            end
          end
        end
        queries
      end

      def get_named_queries(query_name)
        named_queries = []
        queries = @named_queries[query_name]
        return named_queries if queries.nil?
        queries = (queries.is_a? Array) ? queries : [queries]
        queries.each do | query |
          if query.is_a? Symbol
            named_queries.concat(get_named_queries(query))
          else
            named_queries.push(query)
          end
        end
        named_queries
      end

      def extend_query(child)
        parent = get_named_queries(child[:extends])[0]
        child[:entity_type] = child[:entity_type] || parent[:entity_type]
        child[:type] = child[:type] || parent[:type]
        child[:name] = child[:name] || parent[:name]
        child[:select_type] = child[:select_type] || parent[:select_type]
        child[:fields] = concat(child[:fields], parent[:fields])
        child[:grouping] = merge(child[:grouping], parent[:grouping])
        child[:having] = merge(child[:having], parent[:having])
        child[:joins] = concat(child[:joins], parent[:joins])
        child[:includes] = concat(child[:includes], parent[:includes])
        child[:map] = merge(child[:map], parent[:map])
        child[:set_name] = child[:set_name] || parent[:set_name]
        child[:results] = merge(child[:results], parent[:results])
        child[:conditions] = concat(child[:conditions], parent[:conditions])
        child[:params] = merge(child[:params], parent[:params])
        child[:sort] = concat(child[:sort], parent[:sort])
        child[:limit] = child[:limit] || parent[:limit]
        child[:offset] = child[:offset] || parent[:offset]
        child[:distinct] = child[:distinct] || parent[:distinct]
        child[:transforms] = concat(child[:transforms], parent[:transforms])
        child
      end

      def merge_criteria(to, from)
        if (from)
          c = Entities::CriteriaEntity.new
          c.entity_type = from[:entity_type] || self.name
          c.select_type = from[:select_type] || :values
          c.fields = from[:fields]
          c.grouping = from[:grouping]
          c.having = from[:having]
          c.joins = from[:joins]
          c.includes = from[:includes]
          c.map = from[:map]
          c.set_name = from[:set_name]
          c.distinct = from[:distinct]
          c.results = from[:results]
          c.conditions = from[:conditions]
          c.transforms = from[:transforms] || []

          c.params = @resolver.resolve(from[:params], to.params, TypeResolver.resolve_domain_type(c.entity_type || self.name))
          c.type = from[:type] || to.type
          c.name = to.name
          c.sort = to.sort || from[:sort]
          c.limit = to.limit || from[:limit]
          c.offset = to.offset || from[:offset]
          criteria = c
        else
          criteria = to
        end
        criteria
      end

      def execute_query(type, criteria)
        data = { }
        fields = criteria.fields || type.column_names.map(&:to_sym)
        fields_map = criteria.map || default_map(fields, criteria.select_type)
        query = type.where(where(type.arel_table, criteria.params, criteria.conditions))
                    .joins(join(criteria.joins, criteria.params))
                    .includes(criteria.includes)
                    .group(criteria.grouping)
                    .having(criteria.having)
                    .order(criteria.sort)
                    .limit(criteria.limit)
                    .offset(criteria.offset)
                    .distinct(criteria.distinct)

        if (criteria.select_type == :static)
          entities = criteria.results
        elsif (criteria.select_type == :model)
          entities = query.select(*fields)
                         .map { | model | @mapper.map(model, fields_map)}
        else
          entities = query.pluck(*fields)
                         .map { | values | @mapper.map(values, fields_map)}
        end

        entities = transform(criteria.transforms, entities) if criteria.transforms

        name = criteria.set_name || type.name.demodulize.pluralize.underscore
        data[name.to_sym] = entities
        data
      rescue StandardError => err
        Rails.logger.error err.message
        Rails.logger.error err.backtrace
        @results.error(err.message, criteria)
        data
      end

      def default_map(fields, select_type)
        select_type == :model ?
            Hash[fields.collect { | field | [field, field] }]
        : Hash[fields.map.with_index { | field, index | [field, :"#{index}"] }]
      end

      def transform(transforms, data)
        transforms.each do | transform |
          transformer = Processors::ProcessorFactory.get_processor('transforms', "#{transform}_transform")
          data = transformer.new.process(data) if transformer
        end
        data
      end

      def join(joins, params)
        return nil if joins.nil?
        formatted_joins = []
        joins.each do | join |
          if (join.kind_of?(CustomJoin))
            #join_type = join[:join_type] == :outer ? Arel::Nodes::OuterJoin : Arel::Nodes::InnerJoin #FOR LATER
            join_type = join[:join_type] == :outer ? 'LEFT' : 'INNER'
            join_table = (join[:join_table].is_a? Symbol) ? params[join[:join_table]] : join[:join_table]
            joined_table = (join[:joined_table].is_a? Symbol) ? params[join[:joined_table]] : join[:joined_table]
            join_field = (join[:join_field].is_a? Symbol) ? params[join[:join_field]] : join[:join_field]
            joined_field = (join[:joined_field].is_a? Symbol) ? params[join[:joined_field]] : join[:joined_field]
            #join = join_table.join(joined_table, join_type).on(join_table[join_field].eq(joined_table[joined_field])).join_sources  #FOR LATER
            join = "#{join_type} JOIN #{join_table} ON `#{joined_table}`.`#{joined_field}` = `#{join_table}`.`#{join_field}`"
          end
          formatted_joins.push(join)
        end
        formatted_joins
      end

      def where(arel, params, conditions)
        return params.to_h if conditions.nil?
        nodes = nil
        conditions.each do | condition |
          if nodes.nil?
            nodes = build_condition(nodes, arel, params, condition)
          else
            nodes = nodes.and build_condition(nodes, arel, params, condition)
          end
        end
        nodes
      end

      def build_condition(nodes, arel, params, condition)
        if (condition[FIELD].is_a? String)
          if condition[FIELD].include?('.')
            parts = condition[FIELD].split('.')
            table = Arel::Table.new(parts[0])
            nodes = table[parts[1]].send(condition[OPERATOR], params[condition[VALUE]])
          end
        else
          nodes = arel[condition[FIELD]].send(condition[OPERATOR], params[condition[VALUE]])
        end
        if (condition[SUB_CONDITION])
          nodes = nodes.or where(arel, params, condition[SUB_CONDITION])
        end
        nodes
      end

      FIELD = 0; OPERATOR = 1; VALUE = 2; SUB_CONDITION = 3

      private

      def merge(this, that)
        this ? that ? this.merge(that) : this : that
      end

      def concat(this, that)
        this ? that ? this.concat(that) : this : that
      end

    end

  end
end
