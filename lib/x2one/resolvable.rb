module X2one
  class Resolvable < Callable
    def resolve(context, input)
      @callable.call(context, input)
    end
  end

  class ResolvableInput
    attr_accessor :type
    attr_accessor :field
    attr_accessor :bindings

    def initialize(type, field, bindings = {})
      self.type = type
      self.field = field
      self.bindings = bindings
    end
  end
end

