module X2one
  module Resolvers
    class ParameterResolver
      attr_accessor :context

      def initialize(context)
        @context = context
      end

      def resolve(parms, bindings, domain_type)
        resolved = {}
        if parms.is_a? Array #resolve a list of parameters sets
          parms.each do |parm|
            p = resolve(parm, bindings, domain_type)
            resolved = resolved.merge(p)
          end
        elsif (parms == :none) #do not use any parameters
          resolved = {}
        elsif (parms.nil?) #use the bindings as is
          resolved = bindings
        elsif (parms.is_a? X2one::Resolvables) #resolve to a parameter set
          resolved = parms.resolve(context, ResolvableInput.new(domain_type, parms, bindings))
        else # resolve each parameter
          parms.each do |parm, value|
            resolved[parm] = resolve_param(parm, value, bindings, domain_type)
          end
        end
        resolved
      end

      def resolve_param(parm, value, bindings, domain_type)
        if value.is_a? Array #resolve a default list
          val = nil
          value.each do |option|
            val = resolve_param(parm, option, bindings, domain_type)
            break if !val.nil?
          end
          val
        elsif value.is_a? Resolvable #resolve
          value.resolve(context, ResolvableInput.new(domain_type, parm, bindings))
        elsif value.is_a? Symbol #to a field value
          bindings.nil? ? nil : bindings.get(parm)
        else #to a constant
          value
        end
      end

    end
  end
end
