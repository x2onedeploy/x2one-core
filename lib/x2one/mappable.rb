module X2one
  class Mappable < Callable
    def map(data, field = nil, context = nil)

      @callable.call(data, field, context)
    end
  end
end
