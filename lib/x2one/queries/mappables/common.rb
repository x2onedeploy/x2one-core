module X2one
  module Queries
    module Mappables
      module Common
        # EXAMPLE = Mappable.new { | data, field, context |
        #   "#{data.first_name} #{data.last_name}"
        # }
        PRINCIPAL  = Mappable.new{|data| (data.principal.present? ? {id: data.principal.id,
                                                                     name: "#{I18n.t((data.principal.entity_type).underscore.to_sym)} - #{data.principal.entity.name}" ,
                                                                     entity_name: data.principal.entity.name,
                                                                     entity_type: {type: data.principal.entity_type, name:I18n.t((data.principal.entity_type).underscore.to_sym)}
        } : nil) }

        OWNER_STRING = Mappable.new { |data, field|
          "#{data.owner.class} - #{data.owner.name}" if data.principal.present? && data.owner
        }

        PERMISSION_NAME = Mappable.new { |data|
          Enums::Permissions.flags(data.permission)
        }

        LOCALIZE = Mappable.new { |data,field| I18n.t data[field] }

        SHARED_LIST_MAP = Mappable.new{ |data| data}


        ACTION_MAP = Mappable.new{ |data, key, context|
          Mappers::ArrayMapper.new().map( data,'actions', [{sort_by: :priority}, {group_by: :state}], ACTION_STRING)
        }

        # GENERAL_MAP = Mappable.new{ |data| Processors::Mapping::MapProcessor.new(context).process(data).data}
        #
        # ACTION_STRING = Mappable.new{ |data, key, context|
        #   parts = []
        #   parts.push("#{data['routable_type']} ") if data['routable_type']
        #   parts.push("#{data.routable.name} ") if data.routable.name
        #   parts.push("Timeout #{data['timeout']} Sec") if data['timeout']
        #   parts.join()
        # }
        #
        # CONDITION_STRING = Mappable.new{ |data, key, context|
        #   parts = []
        #   parts.push("#{data['condition_item_type']} - ") if data['condition_item_type']
        #   parts.push("#{data.condition_item.name} ") if data.condition_item.name
        #   parts.join()
        # }
        #
        # AFFECTED_RESOURCE_STRING = Mappable.new{ |data, key, context|
        #   parts = []
        #   parts.push("#{data['affected_type']} - ") if data['affected_type']
        #   parts.push("#{data.affected.name} ") if data.affected.name
        #   parts.join()
        # }
      end
    end
  end
end