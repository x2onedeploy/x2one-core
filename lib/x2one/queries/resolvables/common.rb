module X2one
  module Queries
    module Resolvables
      module Common
        CURRENT_USER = Resolvable.new { |context| context.current_user }
        CURRENT_USER_ID = Resolvable.new { |context| context.current_user.id }
        CURRENT_DOMAIN_USER = Resolvable.new { |context| context.current_domain_user }
        CURRENT_DOMAIN_USER_ID = Resolvable.new { |context| context.current_domain_user.id }
        CURRENT_CUSTOMER_ID = Resolvable.new { |context| context.current_user.customer_id }
        CURRENT_ROLE_ID = Resolvable.new { |context| context.current_role.id }
        CURRENT_BILLING_LINE = Resolvable.new { |context| context.current_billing_line }
        ROLE_TYPE = Resolvable.new { |context| context.role_type }

        NONE = X2one::Resolvables.new { |context| {} }
        SHARED = X2one::Resolvables.new { |context,input| {
            id: Processors::OwnedEntity::Shared::SharedGetProcessor.new(context).process(input.type).data}
        }
      end
    end
  end
end