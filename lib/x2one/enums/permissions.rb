module X2one
  module Enums
    class Pair
      attr_accessor :value
      attr_accessor :name

      def initialize(value, name)
        self.value = value
        self.name = name
      end
    end

    class Enum
      @@values = []

      def self.values
        return @@values
      end

      def self.name(value)
        @@values[value].name
      end

      def self.flags(value)
        @@values.select { | v | !v.nil? && (v.value & value == v.value) }
            .map { | v | v.name }
            .join(' | ')
      end

    end

    class Permissions < Enum
      NONE = 0
      VIEW = 1
      MODIFY = 2
      DELETE = 4
      SHARE = 8
      EXECUTE = 16
      @@values[NONE] = Pair.new(NONE, 'None')
      @@values[VIEW] = Pair.new(VIEW, 'View')
      @@values[MODIFY] = Pair.new(MODIFY, 'Modify')
      @@values[DELETE] = Pair.new(DELETE, 'Delete')
      @@values[SHARE] = Pair.new(SHARE, 'Share')
      @@values[EXECUTE] = Pair.new(EXECUTE, 'Execute')
    end


  end
end

